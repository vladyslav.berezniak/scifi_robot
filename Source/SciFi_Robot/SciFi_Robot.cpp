// Copyright Epic Games, Inc. All Rights Reserved.

#include "SciFi_Robot.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SciFi_Robot, "SciFi_Robot" );
