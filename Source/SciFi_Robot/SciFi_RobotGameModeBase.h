// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SciFi_RobotGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SCIFI_ROBOT_API ASciFi_RobotGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
